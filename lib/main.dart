import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/providers/orders.dart';
import 'package:shop/screens/ManageProductsScreen.dart';
import 'package:shop/screens/EditProductsScreen.dart';
import 'package:shop/screens/cart_screen.dart';
import 'package:shop/screens/orders_screen.dart';
import 'package:shop/widgets/app_drawer.dart';
import 'package:shop/widgets/badge.dart';

import './providers/products.dart';

void main() => runApp(MyApp());

/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/*My mistakes :
	Not returning a widget in Consumer's' builder | Fix : Moved the Consumer a bit up in tree
  In ChangeNotifierProvider(create: (_) => 'existing'-ChangeNotifier-instance) | Fix : Used ChangeNotifierProvider.value(value: existing-ChangeNotifier-instance*/
class MyApp extends StatelessWidget {
  // User breaked onto its properties/members(encapsulated class with ChangeNotifier feature in itself)
  final userFavPdList = UserFavPdList();
  final userCart = UserCart();
  final userAllOrderBagsList = UserAllOrderBagsList();

  @override
  Widget build(BuildContext context) {
    //+++++REQUIRED DATA CONTAINER in diff branches of tree, ATTACHED ON TOP OF TREE
    return MultiProvider(
      providers: [
        // way to expose new Instance
        ChangeNotifierProvider(create: (_) => ProductList1()), //PROPERTIES OF CLASS ProductList1 NOW CAN BE USED IN UNDERLYING CHILD-WIDGETS IN THE TREE
        //way to expose existing Instance
        ChangeNotifierProvider.value(value: userAllOrderBagsList),
        ChangeNotifierProvider.value(value: userFavPdList),
        ChangeNotifierProvider.value(value: userCart),
       ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'MyShop',
          theme: ThemeData(
            primarySwatch: Colors.lightGreen,
            accentColor: Colors.redAccent,
            fontFamily: 'Lato',
          ),
          home: ProductsOverviewScreen(),
          routes: {
            ProductDetailScreen.routeName: (_) => ProductDetailScreen(),
            CartScreen.routeName: (_) => CartScreen(),
            OrdersScreen.routeName: (_) => OrdersScreen(), // although each time we are pushing a new OrderScreen but the state remains the same | although we use new widget on each rebuild but state remains the same
            ManageProductsScreen.routeName: (_) => ManageProductsScreen(),
            AddOrEditProductsScreen.routeName: (_) => AddOrEditProductsScreen()
           }),
    );
  }
}
/*-----------------------------------------------------------------------------------------------------------------------*/
enum ViewOptions {
  Favourites,
  All,
}

class ProductsOverviewScreen extends StatefulWidget {
  const ProductsOverviewScreen({Key key}) : super(key: key);

  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  ViewOptions v = ViewOptions.All;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: AppDrawer(),
      appBar: AppBar(
        title: Text('MyShop'),
        actions: <Widget>[
          PopupMenuButton(
            tooltip: 'Select View',
            itemBuilder: (_) => [
              PopupMenuItem(
                child: Text('Show All'),
                value: ViewOptions.All,
              ),
              PopupMenuItem(
                child: Text('Show Favourites'),
                value: ViewOptions.Favourites,
              ),
            ],
            onSelected: (view) {
              if (v != view)
              setState(() {
                this.v = view;
              });
            },
            onCanceled: () {},
          ),
          Consumer<UserCart>(
            child: IconButton(icon: Icon(Icons.shopping_cart), onPressed: () {Navigator.pushNamed(context, CartScreen.routeName);},),
            builder: (_, cart, child) => Badge(
                child: child, // a widget inside Consumer.builder's widget that you don't want to rebuild
                value: cart.itemCount.toString()),
          )
        ],
      ),
//-------- FROM HERE ON THE PRODUCT-LIST IS NEEDED
      body: ProductsGrid(v),
    );
  }
}

/*---------------------------------------------------------------------------------------------------------------------------*/
class ProductsGrid extends StatelessWidget {
 final ViewOptions v;
 const ProductsGrid(this.v);
/// DISPLAYING A LIST OF PRODUCTS
  @override
  Widget build(BuildContext context) {
//======= PRODUCT-LIST ACCESSED
// access the data from ProductList1 then listen to any changes in ProductList1, on notifyListeners() from ProductList1 rebuild this whole context
//    final products = Provider.of<ProductList1>(context, listen: true);
    final List<Product> allPL = Provider.of<ProductList1>(context, listen: true).pL;
    final List<Product> favPL = Provider.of<UserFavPdList>(context, listen: true).favProducts;
    final List<Product> displayedPdList = v == ViewOptions.All ? allPL : favPL;
    return GridView.builder(
      padding: const EdgeInsets.all(10.0),
      itemCount: displayedPdList.length,
      itemBuilder: (_, i) => ChangeNotifierProvider.value(
        //+++++REQUIRED DATA CONTAINER ATTACHED, ON TOP OF TREE
        value: displayedPdList[i], // THE existing PRODUCT-instance in productList is exposed and NOW CAN BE USED & Listened IN UNDERLYING CHILD-WIDGETS IN THE TREE
//------ FROM HERE ON ONLY PRODUCT FROM PRODUCT-LIST IS NEEDED
        child: ProductItemWidget(),
      ),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
      ),
    );
  }
}

/*---------------------------------------------------------------------------------------------------------------------*/
/// THIS WIDGET CAN'T BE USED AGAIN, IF THE PRODUCT IT REQUIRES DOESN'T HAS FEATURES FROM ChangeNotifier
class ProductItemWidget extends StatelessWidget {
  const ProductItemWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
//======== PRODUCT ACCESSED
// if you just want to get data from your global data storage, but not interested in updates(Means you want to use it as an API), then you don't need to listen
// by setting listen to false, it will not rebuild its context, when notifyListeners() happen from the Product
    final pdct = Provider.of<Product>(context, listen: false);
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: InkWell(
          onTap: () {
            Navigator.of(context).pushNamed(
              ProductDetailScreen.routeName,
              arguments: pdct.id,
            );
          },
          child: Image.network(
            pdct.imageUrl,
            fit: BoxFit.cover,
          ),
        ),
        footer: GridTileBar(
          backgroundColor: Colors.black87,
          leading: Consumer<UserFavPdList>(
            builder: (_, uFL, constChild) => IconButton(
              icon: Icon(uFL.isProductFav(pdct) ? Icons.favorite : Icons.favorite_border), // should listen, as need to be rebuild on new icon value
              color: Theme.of(context).accentColor,
              onPressed: () => pdct.toggleFavoriteStatus(uFL)// linked this product's favourite icon to the current UserFavPdList | a member from the current UserFavPdList instance is read & according to that icon is set, so needs to listen for changes in UserFavPdList
            ),// onError : Provider.of<UserFavPdList>(context, listen: false)
          ),
          title: SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Text(
              pdct.title,
              textAlign: TextAlign.center,
              overflow: TextOverflow.clip,
            ),
          ),
          trailing: IconButton(
            tooltip: 'Add to cart',
            icon: Icon(
              Icons.add_shopping_cart,
            ),
            onPressed: () {
              final userCart = Provider.of<UserCart>(context, listen: false);
              userCart.addProduct(pdct.id, pdct.price, pdct.title); // added product to userCart-bloc(which can convey this to corresponding service(will convey this to database)
              Scaffold.of(context) // to show info that pd is added
                ..hideCurrentSnackBar() // if there is a SnackBar already, removes it first show another
                ..showSnackBar(SnackBar(
                  content: Text('Added item to cart!'),
                  duration: Duration(seconds: 2),
                  action: SnackBarAction(label: 'UNDO', onPressed: () {userCart.removeProduct(pdct.id);},),
              ));
              }, // accessed current UserCart instance, to add an Item to it
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
    );
  }
}

/*-------------------------------------------------------------------------------------------------------------------------*/
class ProductDetailScreen extends StatelessWidget {
  static const routeName = '/product-detail';
  const ProductDetailScreen({Key key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    final productId = ModalRoute.of(context).settings.arguments as String; // is the id!
    final pd = Provider.of<ProductList1>(context, listen: false).findById(productId); // fetching the product instance | the product list needs to be accessed in many Widget-classes
    return Scaffold(
      appBar: AppBar(
        title: Text(pd.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300,
              width: double.infinity,
              child: Image.network(pd.imageUrl, fit: BoxFit.cover),
            ),
            SizedBox(height: 10),
            Text('${pd.price}',
              style: TextStyle(color: Colors.grey, fontSize: 20),
            ),
            SizedBox(height: 10),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10),
                width: double.infinity,
                child: Text(pd.description, textAlign: TextAlign.center, softWrap: true))
          ],
        ),
      ),
    );
  }
}
/*--------------------------------------------------------------------------------------------------------------------------*/
