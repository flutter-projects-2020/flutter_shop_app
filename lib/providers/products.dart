import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

// const for those variables , whose primitive value you assigned at compile-time(code-writing time) & will not change at runtime
const url = 'https://ecommerceshoptrial.firebaseio.com'; // project's firebase_url

/* For what do you want to manage, create a provider for that
Here we are managing, individual products & a list of products too

Change notifier is related to inherited widget
Inherited widget establishes a communication tunnel with the help of context object , that we get in every widget
* */

// A CLASS THAT USES FEATURES FROM ChangeNotifier
// CAN DIRECTLY NOTIFY TO ITS USERS/LISTENERS, ABOUT A CHANGE IN ITS PROPERTY
// don't directly manipulate data from anywhere else in the app, as in that case you can't notifyListeners(), and widgets that depend on data from this class would not rebuild correctly, because they would not know about the change
// only change data from inside the class, then notifyListeners(), and parts of the app that are listening to this class will get rebuilt
class ProductList1 with ChangeNotifier {
  // A product-instance journey from this list
  // 1st shown on AllProductsGrid (ProductOverviewScreen)
  // If it is favourite, then it is also shown on FavoriteProductsGrid (ProductOverviewScreen)
  // If it gets added to UserCart, then a CartProduct is made out of it & shown on CartScreen
  // If it is ordered, then it goes in a List<CartProduct> of an OrderBag
  /* Product id title description price imageUrl;
     CartProduct id title(==p.title) quantity price(==p.price)
     OrderBag id dateTime cpList amount(==cp.price * cp.quantity)
   */
  List<Product> _pL = [
    Product(
      id: 'p1',
      title: 'Red Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
          'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    ),
    Product(
      id: 'p2',
      title: 'Trousers',
      description: 'A nice pair of trousers.',
      price: 59.99,
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',
    ),
    Product(
      id: 'p3',
      title: 'Yellow Scarf',
      description: 'Warm and cozy - exactly what you need for the winter.',
      price: 19.99,
      imageUrl:
          'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg',
    ),
    Product(
      id: 'p4',
      title: 'A Pan',
      description: 'Prepare any meal you want.',
      price: 49.99,
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
    ),
  ];

  List<Product> get pL => [..._pL];  // a different list, containing copy of elements in _items

  // an add product event, need to save/send new product to server through service, then listen for the update list OR latest added product from there
  // send snapshot of data to server via HTTP
  Future<void> addProduct(Product p) async {
   try { // code that should run if no error happens
     var response = await http.post( //-// data getting added to server | a backEnd-id generated on the server is returned as response in json format {name: product-id}
          '$url/prducts.json', // creates a collection called 'products.json' firebase reads this url & translates it into a database query to create such a collection & store data there //alone doesn't says anything about data you wanna store, it just points to locn of the server where you wanna send your data
          body: jsonEncode({
            't' : p.title,
            'd' : p.description,
            'i' : p.imageUrl,
            'p' : p.price
          })// allows to define the data to send to the web-server that gets attached to the request
      );
     // id of product will be generated in the server, HERE p will not have an id
     final newProduct = Product( // bcoz can't edit properties of a product, so a new Product object
         title: p.title, description: p.description, imageUrl: p.imageUrl, price: p.price,
         id: jsonDecode(response.body)['name'] //now id on frontEnd & backEnd are same|DateTime.now().toString() //todo: until we find a better id
     );
     _pL.insert( //-// data getting added to in memory list of products
         0,
         newProduct
     ); // to add an item in last of List , use List.add
     notifyListeners();
   } on Exception catch (e) {
     print(e);
     throw e;
   }

  }

  void updateProduct(String pId, Product updatedProduct) {
    final pIndex = _pL.indexWhere((p) => p.id == pId); //Returns the first index in the list that satisfies the provided [test]
    if (pIndex == -1) // Returns -1 if [element] is not found.
      {
        debugPrint('Given Product Id not exist in list, so not updated');
    }
    else {
      // This increases the length of the list by one and shifts all objects at or after the index towards the end of the list.
      //_pL.insert(pIndex, updatedProduct);
      // But I want to have the list of same length as it was before, I need to override what's at 'pIndex'
      _pL[pIndex] = updatedProduct;
      notifyListeners();
    }
  }

  void deleteProduct(String pId) {
    _pL.removeWhere((p) => p.id == pId);
    notifyListeners(); // call this method every time you do change in the product-list, so that the widget that depend on this product list , re-render
  }


 //  List<Product> get fPL {
//    return _pL.where((p) => p.isFavorite).toList();
//  }

  Product findById(String id) {
    return _pL.firstWhere((prod) => prod.id == id);
  }


}
// making Product's all fields final , makes it impossible to be edited from any other places you are not aware of
// it can only be edited from place you have set up the logic for it to be edited
class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;


//  bool isFavorite;

   Product({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.price,
    @required this.imageUrl,
//    this.isFavorite = false,
  });

  // '==' operator for object equality | overriding for contains function on the List containing product
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is Product &&
              runtimeType == other.runtimeType &&
              id == other.id &&
              title == other.title &&
              description == other.description &&
              price == other.price &&
              imageUrl == other.imageUrl;
  // Equal objects MUST have the same hash code value as the system depends on this, Objects that are considered equal have same hashcode
  @override
  int get hashCode => id.hashCode ^ title.hashCode ^ description.hashCode ^ price.hashCode ^ imageUrl.hashCode;

  void toggleFavoriteStatus(UserFavPdList uFL) { // toggle favourite status of this product for a specific user
//    isFavorite = !isFavorite;
//    notifyListeners();// A CHANGE IN ANY PROPERTY IN THE CLASS, NEEDS TO NOTIFIED TO ITS RESPECTIVE USER/LISTENER
    uFL.toggle(this);
  }
}

class UserFavPdList with ChangeNotifier {
  List<Product> _favProdList = [];

  List<Product> get favProducts => _favProdList;

  // toggle - if product is not in list add it or vice-versa
  void toggle(Product p) {
    if (isProductFav(p)) _favProdList.remove(p);
    else _favProdList.add(p);
    notifyListeners();
  }

  bool isProductFav(Product p) {
    return _favProdList.contains(p);
  }

}

class CartProduct {
  final String id;
  final String title;
  final int quantity;
  final double price;

  const CartProduct({
    @required this.id, // DateTime.now().toString(),
    @required this.title,
    @required this.quantity, // ** 1 or greater than 1
    @required this.price,
  });
}

class UserCart with ChangeNotifier {



  Map<String, CartProduct> _itemsPidMapCP = {};

  /// Product.id to CartItem
  Map<String, CartProduct> get itemsPidMapCP => {..._itemsPidMapCP}; // spread operator on a map
  /*Product id title description price imageUrl;
    CartProduct id title(==p.title) quantity price(==p.price)
    OrderBag id dateTime cpList amount(==cp.price * cp.quantity)
  * */

  /// generates CartProduct from Product, then updates the UserCart
  void addProduct( // executed from ProductItemWidget
      String productId,
      double productPrice,
      String productTitle,
      ) {
   /* if (_itemsPidMapCP.containsKey(productId)) {
      _itemsPidMapCP.update( // touch existing keys | // change quantity | it's an error to call this Map.update if key is not present & ifAbsent is not provided
        productId,
            (existingCP) => new CartProduct(
          id: existingCP.id,  //-// cart-product id is 1st product's addition dateTime in cart
          title: existingCP.title,
          price: existingCP.price,
          quantity: existingCP.quantity + 1, //-// quantity updates if key(=product(-id)) exits already | ** CP.quantity can be >1 handled here
        ),
      );
    } else {
      _itemsPidMapCP.putIfAbsent( // not touch existing keys | add new key-value pair
        productId,
            () => CartProduct(
          id: DateTime.now().toString(),
          title: productTitle, //-// cart-product title & price is same as of product
          price: productPrice,
          quantity: 1, // | ** CP.quantity can be 1 handled here
        ),
      );
    } */
   _itemsPidMapCP.update(
     /// if containsKey
     productId, // <-- key
        (CartProduct existingCP) => CartProduct(
       id: existingCP.id,  //-// cart-product id is 1st product's addition dateTime in cart
       title: existingCP.title,
       price: existingCP.price,
       quantity: existingCP.quantity + 1, //-// quantity updates if key(=product(-id)) exits already | ** CP.quantity can be >1 handled here)
   ), /// if not containsKey
     ifAbsent: () => CartProduct(
       id: DateTime.now().toString(),
       title: productTitle, //-// cart-product title & price is same as of product
       price: productPrice,
       quantity: 1, // | ** CP.quantity can be 1 handled here
     ),
   );
    notifyListeners();
  }

  int get itemCount { // shown on badge
    return _itemsPidMapCP.length;
  }

  double get totalAmount {
    double total = 0.0;
    _itemsPidMapCP.forEach(
            (_, cp) {total += cp.price * cp.quantity;}
    );
    return total;
  }

  void removeCartProduct(String productId) { // executed from CartProductWidget
    _itemsPidMapCP.remove(productId);
    notifyListeners();
  }


  void removeProduct(String productId) {
    // if the product is not part of cart, nothing to do | check if the key is not present
    if(!_itemsPidMapCP.containsKey(productId)) return;
    // from here on we know product is part of the cart, so there will be no error even if we not give 'ifAbsent' to Map.update
    // if the product quantity is greater than 1, reduce its quantity
    if(_itemsPidMapCP[productId].quantity > 1)
      _itemsPidMapCP.update(productId, (existingCP) =>  CartProduct(
        id: existingCP.id,  //-// cart-product id is 1st product's addition dateTime in cart
        title: existingCP.title,
        price: existingCP.price,
        quantity: existingCP.quantity - 1)); //-// quantity updates if key(=product(-id)) exits already);
    // CP.quantity will be greater than 1 or 1, because we are generating CP in addProduct only on this basis
//    if(_itemsPidMapCP[productId].quantity == 1)
    else _itemsPidMapCP.remove(productId);

    notifyListeners();

  }

  void clear() {
    _itemsPidMapCP.clear();
    notifyListeners();
  }

  bool get isEmpty => _itemsPidMapCP.isEmpty;


}

