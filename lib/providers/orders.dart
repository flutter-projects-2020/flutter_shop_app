import 'package:flutter/foundation.dart';

import './products.dart';

class OrderBag {
  final DateTime dateTime;
  final String id;
  final double amount;
  final List<CartProduct> cpList;

  const OrderBag({
    @required this.dateTime,
    @required this.id,
    @required this.amount,
    @required this.cpList,
  });
}

class UserAllOrderBagsList with ChangeNotifier {
  List<OrderBag> _orderBags = [];
/// a getter but not a setter, so that we don't directly start adding item from outside of this class without notifying-listeners, any change in this class variables should happen from inside of this class , so that this class could take appropriate actions
  List<OrderBag> get orderBags => [..._orderBags];

  // A new Bag gets generated on each order with id of dateTime it was ordered
  void addBag(List<CartProduct> cpList, double totalAmount) {
    _orderBags.insert(
      0,
      OrderBag(
        id: DateTime.now().toString(),
        cpList: cpList,
        amount: totalAmount,
        dateTime: DateTime.now(),
      ),
    );
    notifyListeners();
  }
}
