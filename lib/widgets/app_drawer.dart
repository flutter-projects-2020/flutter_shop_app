import 'package:flutter/material.dart';
import 'package:shop/screens/ManageProductsScreen.dart';

import '../screens/orders_screen.dart';

class AppDrawer extends StatelessWidget {
  const AppDrawer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          AppBar(
            title: Text('Hello Satya!'),
            automaticallyImplyLeading: false,
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.shop),
            title: Text('My Shop'),
            onTap: () {
              Navigator.of(context).pushReplacementNamed('/');
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.payment),
            title: Text('My Orders'),
            onTap: () {
              Navigator.of(context).pushNamed(OrdersScreen.routeName);
            },
          ),
          Divider(),
          ListTile(
            leading: Icon(Icons.edit),
            title: Text('Manage Products'),
            onTap: () {
              Navigator.of(context).pushNamed(ManageProductsScreen.routeName);
            },
          ),
          Expanded(
            child: Align(
              alignment: Alignment.bottomCenter,
                child: SatyaEmblem()
            ),
          )
        ],
      ),
    );
  }
}

class SatyaEmblem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Text("\$atya",
          style: TextStyle(
            letterSpacing: 2.0,
            color: Colors.black45,
            fontSize: 20.0,
            fontWeight: FontWeight.bold,
            fontStyle: FontStyle.italic,
            shadows: [
              Shadow(
                  blurRadius: 3.0,
                  color: Colors.greenAccent,
                  offset: Offset(3.0, 3.0)),
              Shadow(
                  color: Colors.redAccent,
                  blurRadius: 3.0,
                  offset: Offset(-3.0, 3.0)),
            ],
          )),
    );
  }
}
/*Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Theme.of(context).accentColor,
            child: Text(
              'Cooking Up!',
              style: TextStyle(
                  fontWeight: FontWeight.w900,
                  fontSize: 30,
                  color: Theme.of(context).primaryColor),
            ),
          ),
          */
