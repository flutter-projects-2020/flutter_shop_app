import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/products.dart';
import '../providers/orders.dart';

class CartScreen extends StatelessWidget {
  static const routeName = '/cart';
  const CartScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userCart = Provider.of<UserCart>(context, listen: true); // a listener, rebuild the whole context when it listens from this Cart instance
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Cart'),
      ),
      body: Column(
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(15),
            child: Padding(
              padding: EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Total',
                    style: TextStyle(fontSize: 20),
                  ),
                  Spacer(),
                  Chip(
                    label: Text('₹${userCart.totalAmount.toStringAsFixed(2)}',
                      style: TextStyle(
                        color: Theme.of(context).primaryTextTheme.headline6.color,
                      ),
                    ),
                    backgroundColor: Theme.of(context).primaryColor,
                  ),
                  FlatButton(
                    child: Text('ORDER NOW'),
                    onPressed: () {
                      if (!userCart.isEmpty)
                      {
                        Provider.of<UserAllOrderBagsList>(
                            context, listen: false).addBag(
                          userCart.itemsPidMapCP.values.toList(),
                          userCart.totalAmount,
                        );
                        userCart.clear();
                      }
                    },
                    textColor: Theme.of(context).primaryColor,
                  )
                ],
              ),
            ),
          ),
          SizedBox(height: 10),
          Flexible(
            child: ListView.builder(
              itemCount: userCart.itemsPidMapCP.length,
              itemBuilder: (_, i) {
                CartProduct cp = userCart.itemsPidMapCP.values.toList()[i];
                String pId = userCart.itemsPidMapCP.keys.toList()[i];
                return CartProductWidget(
                  productId: pId, // product-Id is transferred, so that respective CartProduct could be removed UserCart.remove(productId)
                  title: cp.title,
                  id: cp.id,
                  price: cp.price,
                  quantity: cp.quantity,
                  );
                },
            ),
          )
        ],
      ),
    );
  }
}
/*------------------------------------------------------------------------------------------------------------------------------------------------*/
/// THIS WIDGET CAN BE USED AGAIN, because the data it depends on can be passed to it through constructor
class CartProductWidget extends StatelessWidget {
  final String id;
  final String productId;
  final double price;
  final int quantity;
  final String title;

  const CartProductWidget({
    @required this.productId,
    @required this.id,
    @required this.price,
    @required this.quantity,
    @required this.title,
  });
  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: ValueKey(id),
      background: Container(
        color: Theme.of(context).errorColor,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 20),
        margin: EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 4,
        ),
      ),
      direction: DismissDirection.endToStart,
// If provider way was not used here to access the required member from current UserCart instance, then this removeItem function had to be passed to this class through constructor, and then setState should be called to build much larger part of the app
      onDismissed: (DismissDirection direction) {
        Provider.of<UserCart>(context, listen: false).removeCartProduct(productId);
      },
      confirmDismiss: (DismissDirection direction) { // needs a Future<bool> to be returned
        return showDialog<bool>(context: context, // Returns a [Future] that resolves to the value (if any) that was passed to [Navigator.pop] when the dialog was closed.
            builder: (_) => AlertDialog(
              title: Text('Are you sure?'),
              content: Text('Do you want to remove the item from the cart?'),
              actions: <Widget>[
                FlatButton.icon(onPressed: () {Navigator.of(context).pop(true);}, icon: Icon(Icons.check, color: Colors.green,), label: Text('Yes')),
                FlatButton.icon(onPressed: () {Navigator.of(context).pop(false);}, icon: Icon(Icons.clear, color: Colors.red,), label: Text('No')),
              ],
            ));
      },

      child: Card(
        margin: EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 4,
        ),
        child: Padding(
          padding: EdgeInsets.all(8),
          child: ListTile(
            leading: CircleAvatar(
              child: Padding(
                padding: EdgeInsets.all(5),
                child: FittedBox(
                  child: Text('₹$price'),
                ),
              ),
            ),
            title: Text(title),
            subtitle: Text('Total: ₹${(price * quantity)}'),
            trailing: Text('x $quantity'),
          ),
        ),
      ),
    );
  }
}
