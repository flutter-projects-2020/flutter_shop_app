import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/product.dart';
import '../providers/products.dart';

class AddOrEditProductsScreen extends StatefulWidget {
  static const routeName = '/edit-product';
  const AddOrEditProductsScreen({Key key}) : super(key: key);
  @override
  _AddOrEditProductsScreenState createState() => _AddOrEditProductsScreenState();
}

class _AddOrEditProductsScreenState extends State<AddOrEditProductsScreen> {
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageUrlFocusNode = FocusNode();
  final _imageUrlCtr = TextEditingController();
  // you don't need a controller,
  // in case of a Form if you want the values from each FormField when the Submit button is tapped
  // in case you want values from field before its submit, you can use controller or onChanged

  final _formKey = GlobalKey<FormState>();
  /// in case of new addition of a product
  var _productToEdit = Product( //a blank product, , used with fields, then onSubmit used to make a real-correct new Product object
    id: null, // pId to be generated on server,
    title: '',
    price: 0,
    description: '',
    imageUrl: '',
  );

  var _initialFieldValue = {                       //-//to pre-populate each TextField in case we are adding a new product
    'title': '',
    'description': '',
    'price': '',
//    'imageUrl': '', Initial value for a TextField in case of giving it a controller is already ''(an empty string)
// a TextEditingController is specified for Url field HERE, so initial value OTHER THAN ''(an empty string) for such field will be set using its controller
  // In case of not giving a TextField an explicit TextEditingController then implicitly one is generated with given intial value( in case intial value is also not given then an '')
  };
  var _isInit = true;
  @override
  void didChangeDependencies() {
    /// although a Product is final, here is the logic to update an existing product, by making a new Product with same id

    if (_isInit) { // we only need to extract product id from previous screen, so running this only 1 time ever
      final productId = ModalRoute.of(context).settings.arguments as String;
      if (productId != null) {
        _productToEdit = Provider.of<ProductList1>(context, listen: false).findById(productId); // productToEdit will have a product id, from the existing product whose copy productToEdit is
        _initialFieldValue = {                     //-//to pre-populate each TextField in case we are editing(updating) an existing product
          'title': _productToEdit.title,
          'description': _productToEdit.description,
          'price': _productToEdit.price.toString(),
//        'imageUrl': _editedProduct.imageUrl, // initial value other than '' for a TextField having explicit controller
//          'imageUrl': '',
        };
        _imageUrlCtr.text = _productToEdit.imageUrl; // initial value for the url field in case we are updating an existing product
      }
    }
    _isInit = false;
    super.didChangeDependencies();
  }
  @override
  void initState() { // best place to initiate a listener, is as soon as the object is initialized, in a classes's constructor or in case of State object in its initState
    _imageUrlFocusNode.addListener( // the given callback executes when focus(focusNode) of the specified field changes
            _updateImageUrl);
    super.initState();
  }

  void _updateImageUrl() {if (!_imageUrlFocusNode.hasFocus) {
    if ( // conditions to check for invalid url, any other conditions will call setState, _imageUrlCtr.text.isEmpty will call setState, which will cause to remove the image below if url input is cleared by the user
    (_imageUrlCtr.text.startsWith('http') || _imageUrlCtr.text.startsWith('https'))
        &&  (_imageUrlCtr.text.endsWith('.png') || _imageUrlCtr.text.endsWith('.jpg') || _imageUrlCtr.text.endsWith('.jpeg'))
    ) {
      setState(() {}); // rebuild to load the url, in case user clicks on different field, while giving input on url field
    } else return;
  }}

  var _interactingWithDatabase = false;
  Future<void> _onSubmit() async { // todo: we wanna send an HTTP request when onSubmit
    final pL1 = Provider.of<ProductList1>(context, listen: false);
    // _formKey.currentState.validate() will run all the validate methods in the FormField, returns true if all field's validate method return null
    if (!_formKey.currentState.validate()) {
      // todo: request focus for the field which fail validation
      return;}
    _formKey.currentState.save(); // to call onSaved method on each FormField in Form
    setState(() {
      _interactingWithDatabase = true; // to indicate long process has started
    });
    if (_productToEdit.id == null) { // good criteria to find out we are editing(updating) or adding a product | if product has an id than it existed before, means we are editing(updating)
      try {
        await pL1.addProduct(_productToEdit); // new product will not have an id HERE
      } on Exception catch (e) {
        debugPrint(e.toString()); // error info goes to logs
        await showDialog(context: context, // showDialog route completes with a result null
        builder: (c) => AlertDialog(
          title: Text('An error occurred!'), // e.toString can reveal confidential info here to users
          content: Text('Something went wrong.'),
          actions: <Widget>[
            FlatButton(child: Text('Okay'),
            onPressed: () {Navigator.pop(c);},)
          ],
        ));
      } finally {
        setState(() {_interactingWithDatabase = false;});
        Navigator.of(context).pop(); // removes form from view
      }
    } else {
      pL1.updateProduct(_productToEdit.id, _productToEdit); // existing product will have an id HERE
      setState(() {_interactingWithDatabase = false;});
      Navigator.of(context).pop(); // removes form from view
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Product'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: _onSubmit,
          ),
        ],
      ),
      body: _interactingWithDatabase ? Center(child: CircularProgressIndicator(),)
      : Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              TextFormField( // initially has an 1. empty default value if we are adding a new product; 2. existing value of the specific property to be edited of the Product
                initialValue: _initialFieldValue['title'],
                decoration: InputDecoration(labelText: 'Title'),
                textInputAction: TextInputAction.next,
                onFieldSubmitted: (_) {FocusScope.of(context).requestFocus(_priceFocusNode);},
                validator: (String userInput) {
                  if (userInput.isEmpty) return 'Please provide a product title.'; // 70773 text
                  return null; // there is no error , userInput can be accepted
                },
                onSaved: (String titleInput) { // on Each userInput create a new identical Product to productToEdit with only userInput property's value changed
                  _productToEdit = Product(
                      title: titleInput,
                      price: _productToEdit.price,
                      description: _productToEdit.description,
                      imageUrl: _productToEdit.imageUrl,
                      id: _productToEdit.id, // null in case of Adding a product | not null in case of Editing(Updating) a Product
                  );
                },
              ),
              TextFormField(
                initialValue: _initialFieldValue['price'],
                decoration: InputDecoration(labelText: 'Price'),
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                focusNode: _priceFocusNode,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(_descriptionFocusNode);
                },
                validator: (String priceInput) { // every TextField always gives a string value as userInput
                  if (priceInput.isEmpty) {
                    return 'Please enter a price.';
                  }
                  if (double.tryParse(priceInput) == null) {
                    return 'Please enter a valid number.';
                  }
                  if (double.parse(priceInput) <= 0) {
                    return 'Please enter a number greater than zero.';
                  }
                  return null;
                },
                onSaved: (String priceInput) {
                  _productToEdit = Product(
                      title: _productToEdit.title,
                      price: double.parse(priceInput),
                      description: _productToEdit.description,
                      imageUrl: _productToEdit.imageUrl,
                      id: _productToEdit.id,
                  );
                },
              ),
              TextFormField(
                initialValue: _initialFieldValue['description'],
                decoration: InputDecoration(labelText: 'Description'),
                maxLines: 3,
                keyboardType: TextInputType.multiline,
                focusNode: _descriptionFocusNode,
                validator: (String descriptionInput) {
                  if (descriptionInput.isEmpty) {
                    return 'Please enter a description.';
                  }
                  if (descriptionInput.length < 10) {
                    return 'Should be at least 10 characters long.';
                  }
                  return null;
                },
                onSaved: (String descriptionInput) {
                  _productToEdit = Product(
                    title: _productToEdit.title,
                    price: _productToEdit.price,
                    description: descriptionInput,
                    imageUrl: _productToEdit.imageUrl,
                    id: _productToEdit.id,
                  );
                },
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    width: 100,
                    height: 100,
                    margin: EdgeInsets.only(
                      top: 8,
                      right: 10,
                    ),
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: Colors.grey,
                      ),
                    ),
                    child: _imageUrlCtr.text.isEmpty
                        ? Text('Enter a URL')
                        : FittedBox(
                      child: Image.network(
                        _imageUrlCtr.text,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Expanded( // todo: try Flexible here
                    child: TextFormField(
//                    initialValue: _initValues['imageUrl'], // When a [controller] is specified, [initialValue] must be null
                      controller: _imageUrlCtr, // in such case , set the controller to an initial value
                      focusNode: _imageUrlFocusNode,
                      decoration: InputDecoration(labelText: 'Image URL'),
                      keyboardType: TextInputType.url,
                      textInputAction: TextInputAction.done,
                      onFieldSubmitted: (_) {
                        _onSubmit();
                      },
                      validator: (String urlInput) {
                        if (urlInput.isEmpty) { //  conditions for invalid input
                          return 'Please enter an image URL.';
                        }
                        if (!(urlInput.startsWith('http') || urlInput.startsWith('https'))) {
                          return 'Please enter a valid URL.';
                        }
                        if (!(urlInput.endsWith('.png') || urlInput.endsWith('.jpg') || urlInput.endsWith('.jpeg'))) {
                          return 'Please enter a valid image URL.';
                        }
                        return null;
                      },
                      onSaved: (String urlInput) {
                        _productToEdit = Product(
                          title: _productToEdit.title,
                          price: _productToEdit.price,
                          description: _productToEdit.description,
                          imageUrl: urlInput,
                          id: _productToEdit.id,
                        );
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _imageUrlFocusNode.removeListener(_updateImageUrl);
    _priceFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _imageUrlCtr.dispose();
    _imageUrlFocusNode.dispose();
    super.dispose();
  }
}

/*
2
void main() {
 _updateImageUrl('https://bla.com/file.png');

 print(validator('https://bla.com/file.png'));

_updateImageUrl2('https://bla.com/file.png');

 print(validator2('https://bla.com/file.png'));

​
}
 void _updateImageUrl(String text) {

      if (
        (text.startsWith('http') || text.startsWith('https')) &&
        (text.endsWith('.png') || text.endsWith('.jpg') || text.endsWith('.jpeg'))
      ) {
        print('Valid Url'); return;
    }
   print('InValid url');
 }
​
String validator(String urlInput) {
                        if (urlInput.isEmpty) { //  conditions for invalid input
                          return 'Please enter an image URL.';
                        }
                        if (!(urlInput.startsWith('http') ||
                            urlInput.startsWith('https'))) {
                          return 'Please enter a valid URL.'; // here to stop this from running need to make the codn above false
                        }
                        if (!(urlInput.endsWith('.png') || // any of the expressions from 3 or exp become true, apna kaam ho gya
                            urlInput.endsWith('.jpg') ||
                            urlInput.endsWith('.jpeg'))) {
                          return 'Please enter a valid image URL.';
                        }
                        return 'Url Passed';
                      }
​
//-- here think as to make false bubble up and then pulling it out, here a expression having '!' will become false if its true(false bubbled), to pull it out we will use && with its partners
 void _updateImageUrl2(String text) {

      if (
        (!text.startsWith('http') && !text.startsWith('https')) || // dono me se kisi ek exp se v false aa gya to kaam ho gya
        (!text.endsWith('.png') && !text.endsWith('.jpg') && !text.endsWith('.jpeg'))
      ) {
        print('InValid Url'); return;
    }
   print('Valid url');
 }

 void _updateImageUrl3(String text) {

      if (
        (!(text.startsWith('http') || text.startsWith('https'))) || // dono me se kisi ek exp se v false aa gya to kaam ho gya
        (!(text.endsWith('.png') || text.endsWith('.jpg') || text.endsWith('.jpeg')))
      ) {
        print('InValid Url'); return;
    }
   print('Valid url');
 }
​
String validator2(String urlInput) {
                        if (urlInput.isEmpty) { //  conditions for invalid input
                          return 'Please enter an image URL.';
                        }
                        if (!urlInput.startsWith('http') &&
                            !urlInput.startsWith('https')) {
                          return 'Please enter a valid URL.'; // here to stop this from running need to make the codn above false, means need to pull out the false after it bubble
                        }
                        if (!urlInput.endsWith('.png') &&
                            !urlInput.endsWith('.jpg') &&
                            !urlInput.endsWith('.jpeg')) {
                          return 'Please enter a valid image URL.';
                        }
                        return 'Url Passed';
                      }
Console
Valid Url
Url Passed
Valid url
Url Passed
*/