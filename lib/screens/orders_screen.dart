import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../providers/orders.dart';
import 'dart:math';
import 'package:intl/intl.dart';
import '../widgets/app_drawer.dart';

class OrdersScreen extends StatelessWidget {
  static const routeName = '/orders';
  const OrdersScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final uOrderBagList = Provider.of<UserAllOrderBagsList>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Orders'),
      ),
//      drawer: AppDrawer(), to have a back button
      body: ListView.builder(
        itemCount: uOrderBagList.orderBags.length,
        itemBuilder: (ctx, i) => OrderBagWidget(uOrderBagList.orderBags[i]),
      ),
    );
  }
}
/*------------------------------------------------------------------------------------------------------------------------------------------------*/
class OrderBagWidget extends StatefulWidget {
  final OrderBag orderBag;

  const OrderBagWidget(this.orderBag);

  @override
  _OrderBagWidgetState createState() => _OrderBagWidgetState();
}

class _OrderBagWidgetState extends State<OrderBagWidget> {
  bool _expanded = false;
  OrderBag ob;
  @override
  void initState() {
    super.initState();
    ob = widget.orderBag;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text('₹${ob.amount}'),
            subtitle: Text(DateFormat('dd/MM/yyyy hh:mm').format(ob.dateTime)),
            trailing: IconButton(
              icon: Icon(_expanded ? Icons.expand_less : Icons.expand_more),
              onPressed: () {
                setState(() {_expanded = !_expanded;});
              },
            ),
          ),
          if (_expanded)
            Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
              height: min(ob.cpList.length * 20.0 + 10, 100), // max height for the container set to 100
              child: ListView(
                children: ob.cpList
                    .map(
                        (cp) => Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          cp.title,
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text('${cp.quantity}x ${cp.price}',
                          style: TextStyle(
                            fontSize: 18,
                            color: Colors.grey,
                          ),
                        )
                      ],
                    ))
                    .toList(),
              ),
            )
        ],
      ),
    );
  }
}
