import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:shop/providers/products.dart';
import './EditProductsScreen.dart';
import 'package:shop/widgets/app_drawer.dart';



class ManageProductsScreen extends StatelessWidget {
  static const routeName = '/user-products';

  const ManageProductsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pL1 = Provider.of<ProductList1>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Products'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).pushNamed(AddOrEditProductsScreen.routeName);
            },
          ),
        ],
      ),
//      drawer: AppDrawer(),
      body: Padding(
        padding: EdgeInsets.all(8),
        child: ListView.builder(
          itemCount: pL1.pL.length,
          itemBuilder: (_, i) => Column(
            children: [
              ManageableProductItemWidget(
                pL1.pL[i].id,
                pL1.pL[i].title,
                pL1.pL[i].imageUrl,
              ),
              Divider(),
            ],
          ),
        ),
      ),
    );
  }
}


class ManageableProductItemWidget extends StatelessWidget {
  final String id;
  final String title;
  final String imageUrl;

  ManageableProductItemWidget(this.id, this.title, this.imageUrl);

  @override
  Widget build(BuildContext context) {
    final currentTheme = Theme.of(context);
    return ListTile(
      title: Text(title),
      leading: CircleAvatar( // doesn't takes a widget but an image-provider that yields a image
        backgroundImage: NetworkImage(imageUrl, scale: 1.0),
      ),
      trailing: Container( // todo: try to remove this, see if it works
        width: 100,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.edit), // need to know which product you wanna edit , so transfer the product id(with the help of which we can search or load the product)
              onPressed: () {Navigator.of(context).pushNamed(AddOrEditProductsScreen.routeName, arguments: id);}, // id is forwarded as an argument in our routing action here, we can extract it on our next screen
              color: currentTheme.primaryColor,
            ),
            IconButton(
              icon: Icon(Icons.delete), // need to know which product you wanna delete , so transfer the product id
              onPressed: () {Provider.of<ProductList1>(context, listen: false).deleteProduct(id);},
              color: currentTheme.errorColor,
            ),
          ],
        ),
      ),
    );
  }
}